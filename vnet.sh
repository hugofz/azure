#!/bin/bash

# create Bash shell variables
vnetName=Vnet_OCC_ASD_Hugo
vnetAddressPrefix=10.0.9.0/24
nsgName=NSG_OCC_ASD_Hugo
resourceGroup=OCC_ASD_Hugo

# create network security group
az network nsg create --name $nsgName --resource-group $resourceGroup

# create vnet
az network vnet create --name $vnetName --resource-group $resourceGroup --address-prefixes $vnetAddressPrefix

# create subnets and associate with nsg
for ((i=0; i<=15; i++)); do
    if [ $i -eq 0 ]; then
        subnetName="Subnet_Vnet_OCC_ASD_Hugo_admin"
	startAddress=$((i * 16))
    else
        subnetName="Subnet_Vnet_OCC_ASD_Hugo_$i"
    fi

    startAddress=$((i * 16))
    endAddress=$((startAddress + 15))
    subnetAddressPrefix="10.0.9.$startAddress/28"

    # Create subnet
    az network vnet subnet create --name $subnetName --vnet-name $vnetName --resource-group $resourceGroup --address-prefix $subnetAddressPrefix

    # Associate subnet with NSG
    az network vnet subnet update --name $subnetName --vnet-name $vnetName --resource-group $resourceGroup --network-security-group $nsgName
done
